# iac-gitops-batchmon

Project to test GitOps principles with batchmon infrastructure: mainly Prometheus and Alertmanager

## Namespaces/Releases/Envrionments

For ease of development, a `stg` namespace has been created that allows for testing of the chart before promoting the changes to production.

Both releases are deployed on the `IT-Batch Infrastructure - Container` OS Tenant. `prod` release is deployed to `batchmon-prod` cluster and is available on the internal network as `batchmon-prod.cern.ch` while `stg` is on `batchmon-stg` cluster and available at `batchmon-stg.cern.ch`

By default the chart performs the following functionality

- Setup Prometheus server with targets for batch nodes exporters (TODO: should move to prod?)
- Setup alertmanager instance
- Partially setup nginx ingress for prometheus-server and alertmanager without specifying the host/baseUrl
- setup alertmanager configuration global SMTP and HTTP configs
- setup all receivers (email for AFS Full, webhook and email for AFS Overload)
- setup route for AFS Full errors
- define the email templates

Currently, the following are the changes implemented in the specific environment

### Prod

- Prometheus scrape targets are defined (TODO)
- `UserFullAFSFolder` alert is enabled by setting `alerting_rules.yaml` in `serverFiles` values
- Prometheus and Alertmanager ingress bareUrl/host is set
- setup alerting_rules.yml for UserAFSFullFolder alerts
- add all alerting routes

### Stg

- set ingress to `batchmon-stg`
- set stackstorm webhook to point to `batchstorm-dev`
- don't add `alerting_rules.yml` so AFSFull alerts are not generated
- remove user's from the mail receivers and add a `BATCHMON-DEV` flag in the subject to make it easy to filter test emails
- add all alerting routes

## Deployment

In a *pre v1.22* k8s cluster with *NGINX Ingress enabled* (`--labels ingress_controller=nginx`)

```
helm upgrade -i helm-operator fluxcd/helm-operator --namespace flux \
    --set allowNamespace=<stg/prod> \
    --values helm-operator-values.yaml
```

```
helm upgrade -i flux fluxcd/flux --namespace flux \
    --version 1.3.0 \
    --set git.url=https://gitlab.cern.ch/batch-team/infra/iac-gitops-batchmon \
    --set git.branch=<master/stg> \
    --values flux-values.yaml
```

### Setting up ingress

#### Label the nodes as ingress in kubernetes

```
 kubectl get node
NAME                              STATUS   ROLES    AGE   VERSION
mycluster-7xdvoefvuz2l-minion-0   Ready    <none>   1d    v1.11.2
mycluster-7xdvoefvuz2l-minion-1   Ready    <none>   1d    v1.11.2
mycluster-7xdvoefvuz2l-minion-2   Ready    <none>   1d    v1.11.2

$ kubectl label node mycluster-7xdvoefvuz2l-minion-0 role=ingress
$ kubectl label node mycluster-7xdvoefvuz2l-minion-1 role=ingress
```

#### Create DNS alias in LanDB

```
$ openstack server set --property landb-alias=myclusterdns--load-1- mycluster-7xdvoefvuz2l-minion-0
$ openstack server set --property landb-alias=myclusterdns--load-2- mycluster-7xdvoefvuz2l-minion-1
```
