prometheus:
  nodeExporter:
      enabled: false
  pushgateway:
    image:
      repository: registry.cern.ch/quay.io/prometheus/pushgateway
  server:
    image:
      repository: registry.cern.ch/quay.io/prometheus/prometheus
    persistentVolume:
      size: 100Gi
      accessModes:
        - ReadWriteMany
      storageClass: geneva-cephfs-testing
    prefixURL: '/prometheus'
    ingress:
      enabled: true
      ingressClassName: 'nginx'
    service:
      annotations:
        'prometheus.io/scrape': 'true'
        'prometheus.io/path': '/prometheus/metrics'
  extraScrapeConfigs: |
    - job_name: 'batchinfra_pushgateway'
      metrics_path: '/pushgateway/metrics'
      honor_labels: true
      static_configs:
        - targets:
            - 'batchinfra.web.cern.ch:80'
    - job_name: 'azure_cost_mon'
      scrape_interval: 1h
      scrape_timeout: 60s
      static_configs:
        - targets:
            - 'azureprom.cern.ch:8000'
    - job_name: 'condor_sched'
      static_configs:
        - targets:
            - 'bigbird08.cern.ch:9144'
            - 'bigbird10.cern.ch:9144'
            - 'bigbird11.cern.ch:9144'
            - 'bigbird12.cern.ch:9144'
            - 'bigbird13.cern.ch:9144'
            - 'bigbird14.cern.ch:9144'
            - 'bigbird15.cern.ch:9144'
            - 'bigbird16.cern.ch:9144'
            - 'bigbird17.cern.ch:9144'
            - 'bigbird18.cern.ch:9144'
            - 'bigbird19.cern.ch:9144'
            - 'bigbird20.cern.ch:9144'
            - 'bigbird21.cern.ch:9144'
            - 'bigbird22.cern.ch:9144'
            - 'bigbird23.cern.ch:9144'
            - 'bigbird25.cern.ch:9144'
            - 'bigbird26.cern.ch:9144'
            - 'bigbird27.cern.ch:9144'
            - 'bigbird28.cern.ch:9144'
    - job_name: 'slurm_exporter'
      scrape_interval: 30s
      scrape_timeout: 30s
      static_configs:
        - targets:
          - 'slurm-exporter.cern.ch:8080'
    - job_name: 'cloud_notification_consumer'
      scrape_interval: 30s
      scrape_timeout: 30s
      static_configs:
        - targets:
          - 'cnconsumer01.cern.ch:8080'
  alertmanager:
    image:
      repository: registry.cern.ch/quay.io/prometheus/alertmanager
    persistentVolume:
      accessModes:
        - ReadWriteMany
      storageClass: geneva-cephfs-testing
    prefixURL: '/alertmanager'
    ingress:
      enabled: true
      ingressClassName: 'nginx'
      path: "/alertmanager"
    service:
      annotations:
        'prometheus.io/scrape': 'true'
        'prometheus.io/path': '/alertmanager/metrics'
  alertmanagerFiles:
    alertmanager.yml:
      global:
        resolve_timeout: 5m
        smtp_smarthost:  cernmx.cern.ch:25
        smtp_from: no-reply@cern.ch
        http_config:
            tls_config:
                insecure_skip_verify: true
            authorization:
                type: Bearer
                credentials: APIKEY
      receivers:
      # AFS Full alerts
      - name: 'email_users'
        email_configs:
        - to: '{{ template "email.batch.user_notification.recipient" . }}'
          html: '{{ template "email.batch.html" . }}'
          headers:
            subject: 'Full AFS directory causing HTCondor instabilities (Please take action)'
      # AFS Overload alerts
      - name: 'afs_stackstorm_webhook'
        webhook_configs:
        - send_resolved: true
          url: 'https://batchstorm.cern.ch/webhook/alertmanager/'
      # default receiver for catch-all
      - name: 'default-receiver'
        email_configs:
          - to: 'batch-botmails@cern.ch'
      templates:
        - 'user_notifications.tmpl'
      route:
        group_by:
        - alertname
        group_wait: 1m
        group_interval: 5m
        repeat_interval: 1h
        receiver: default-receiver
        routes:
          - group_by:
            - username
            receiver: email_users
            group_wait: 1m
            continue: false
            # enable if extra actions needs to be taken for AFS Full errors
            matchers:
            - alertname = UserFullAFSFolder
          # AFS Overload Errors
          - group_by:
            - alertname
            - account
            - badness
            receiver: afs_stackstorm_webhook
            group_wait: 5m
            continue: true
            matchers:
            - alertname = afs_hotvolume
            - badness =~ "slow|horrible"
          # re-send alert on webhook every hour for "bad" badness
          - group_by:
            - alertname
            - account
            - badness
            receiver: afs_stackstorm_webhook
            group_wait: 5m
            repeat_interval: 1h
            continue: true
            matchers:
            - alertname = afs_hotvolume
            - badness = bad
    # template for AFS Full notification emails
    user_notifications.tmpl: |
      {{ define "email.batch.user_notification.recipient" }}batch-botmails@cern.ch, {{ .GroupLabels.username }}@cern.ch{{ end }}

      {{ define "email.batch.html" }}
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml"  xmlns="http://www.w3.org/1999/xhtml">

      <p>Dear {{ .GroupLabels.username }},</p>
      <p>It seems like a directory in your AFS space used by your HTCondor jobs has filled up.</p>
      <p>Unfortunately this is causing errors in the HTCondor sched that slow down the whole system significantly, also affecting other users on the same schedd.</p>
      <p>The directories in question are:</p>
      <pre>
      {{- $onepath := (index .Alerts 0).Labels.afs_path -}}
      {{- range .Alerts -}}
      <code>{{ .Labels.afs_path }}</code>
      {{- end -}}
      </pre>
      <p>
      Could you please clear them <b>as soon as possible</b>?
      </p>
      <p>
      A detailed description of this problem and some recommendations to solve it can be found in the Batch Service documentation: <a href="https://batchdocs.web.cern.ch/troubleshooting/afs.html#full-afs-folder">https://batchdocs.web.cern.ch/troubleshooting/afs.html#full-afs-folder</a>
      </p>
      <p>
      The quickest way would be to perform the following action first in the affected directories:
      </p>

      {{- $parent_folder := reReplaceAll "/([\\w_%!$@:.,+~-]+|\\.)$" "" $onepath -}}
      <pre>
      <code>mv {{ $onepath }} {{ $onepath }}_full</code>
      <code>mkdir {{ $onepath }}</code>
      </pre>

      <p>This way you can keep working for now, and the system should immediately recover.</p>

      <p>Thank you in advance for taking action quickly. This is an automated e-mail. If you have any question, you use the following link to access our support line:
        <br><a href="https://cern.service-now.com/service-portal?id=service_element&name=batch">Batch Service Support</a>
      </p>

      <p>
      Best regards,
      <br>Batch Service operators</p>
      </html>
      {{ end }}

  configmapReload:
    prometheus:
      image:
        repository: registry.cern.ch/docker.io/jimmidyson/configmap-reload
    alertmanager:
      image:
        repository: registry.cern.ch/docker.io/jimmidyson/configmap-reload
